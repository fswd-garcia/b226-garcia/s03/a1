-- 1. Create a new s03-a1 folder and then create a file named solution.sql and write the SQL code necessary to insert the following records in the tables and to perform the following query.




-- 2. Add the following records to the blog_db database:


INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA","2021-01-01 01:00:00");



INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA","2021-0101 01:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB","2021-01-01 02:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC","2021-01-01 03:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD","2021-01-01 04:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE","2021-01-01 05:00:00");

INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1,"First Code", "Hello World!","2021-01-02 01:00:00");
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1,"Second Code", "Hello Earth!","2021-01-02 02:00:00");
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (2,"Third Code", "Welcome to Mars!","2021-01-02 03:00:00");
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (4,"Forth Code", "Bye bye solar system!","2021-01-02 04:00:00");

-- 3. Get all the post with an Author ID of 1.

SELECT * FROM posts WHERE author_id = 1;

-- 4. Get all the user's email and datetime of creation.

SELECT email, datetime_created FROM users;

-- 5. Update a post's content to "Hello to the people of the Earth!” where its initial content is "Hello Earth!" by using the record's ID.

UPDATE posts SET content = "Hello to the people of the Earth!" WHERE content = "Hello Earth!";

-- 6. Delete the user with an email of "johndoe@gmail.com".

DELETE FROM users WHERE email = "johndoe@gmail.com";

-- Submit your s03 activity by 8:40 pm: WDC040-3 | MySQL CRUD Operations